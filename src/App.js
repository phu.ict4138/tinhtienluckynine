import Home from './pages/Home';

function App() {
    return (
        <div
            style={{
                width: "100%",
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
                background: "#cccccc"
            }}>
            <div
                style={{
                    color: "red",
                    fontSize: 28,
                    fontWeight: "700",
                    marginBottom: 20,
                    marginTop: 20
                }}>LUCKY 9</div>
            <Home />
        </div>
    );
}

export default App;
