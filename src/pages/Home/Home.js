import classNames from "classnames/bind";
import _ from "lodash";
import { useState } from "react";
import Game from "./components/Game";

import styles from "./Home.module.scss";

const cx = classNames.bind(styles);


function Home() {

    const [data, setData] = useState(() => {
        const history = localStorage.getItem("history_");
        if (!history) {
            return [];
        }
        return JSON.parse(history);
    });
    const [numberPlayer, setNumberPlayer] = useState(1);
    const addNewGame = () => {
        let n = Number(numberPlayer);
        if (n <= 0) {
            n = 1
        }
        if (n >= 5) {
            n = 5
        }
        const a = [];
        for (let i = 0; i < n; i++) {
            a.push(i);
        }
        const game = {
            id: Math.random().toString(),
            des: "",
            banker: {
                card1: 0,
                card2: 0,
                card3: 0,
                total: null,
                money: 60000,
                recieve: null,
                trueRecive: null,
                lastMoney: null
            },
            players: a.map(_ => ({
                id: Math.random().toString(),
                card1: 0,
                card2: 0,
                card3: 0,
                total: null,
                result: null,
                money: 30000,
                totalBet: 3000,
                recieve: null,
                trueRecieve: null,
                lastMoney: null
            })),
            bet: 3000,
            pay: 0.1,
            calcu: false
        };
        data.push(game);
        setData([...data]);
    }

    const onDelete = (id) => {
        _.remove(data, i => i.id == id);
        setData([...data]);
    }

    const onCopy = (game) => {
        const index = _.find(data, i => i.id == game.id) + 1;
        const left = data.slice(0, index);
        const right = data.slice(index, data.length);
        left.push({
            ...game,
            id: Math.random().toString()
        });
        setData(JSON.parse(JSON.stringify(left.concat(right))));
    }

    const onDeletePlayer = (game, id) => {
        _.remove(game.players, i => i.id == id);
        setData([...data]);
    }

    const onAddPlayer = (game) => {
        const number = game.players.length;
        if (number < 5) {
            game.players.push({
                id: Math.random().toString(),
                card1: 0,
                card2: 0,
                card3: 0,
                total: null,
                result: null,
                money: 30000,
                totalBet: 3000,
                recieve: null,
                trueRecieve: null,
                lastMoney: null
            });
        }
        setData([...data]);
    }

    const onChangeDes = (game, des) => {
        game.des = des;
        setData([...data]);
    }

    const onChangeBet = (game, bet) => {
        game.bet = bet;
        setData([...data]);
    }

    const onChangePay = (game, pay) => {
        game.pay = pay;
        setData([...data]);
    }

    const onChangeCard1Banker = (game, card) => {
        game.banker.card1 = card;
        setData([...data]);
    }
    const onChangeCard2Banker = (game, card) => {
        game.banker.card2 = card;
        setData([...data]);
    }
    const onChangeCard3Banker = (game, card) => {
        game.banker.card3 = card;
        setData([...data]);
    }

    const onChangeMoneyBanker = (game, money) => {
        game.banker.money = money;
        setData([...data]);
    }

    const onChangeCard1Player = (player, card) => {
        player.card1 = card;
        setData([...data]);
    }
    const onChangeCard2Player = (player, card) => {
        player.card2 = card;
        setData([...data]);
    }
    const onChangeCard3Player = (player, card) => {
        player.card3 = card;
        setData([...data]);
    }

    const onChangeMoneyPlayer = (player, money) => {
        player.money = money;
        setData([...data]);
    }

    const onChangeBetUser = (player, totalBet) => {
        player.totalBet = totalBet;
        setData([...data]);
    }

    const calculate = (game) => {
        const banker = game.banker
        console.log(game)
        const bankerPoint = (Number(banker.card1) + Number(banker.card2)) % 10 == 9 ? "L9" : (Number(banker.card1) + Number(banker.card2) + Number(banker.card3)) % 10;
        banker.total = bankerPoint;
        for (const player of game.players) {
            const playerPoint = (Number(player.card1) + Number(player.card2)) % 10 == 9 ? "L9" : (Number(player.card1) + Number(player.card2) + Number(player.card3)) % 10;
            let result = "Thua";
            if (bankerPoint == "L9") {
                if (playerPoint == "L9") {
                    result = "Hòa";
                }
                else {
                    result = "Thua";
                }
            }
            else {
                if (playerPoint == "L9") {
                    result = "Thắng";
                }
                else {
                    if (playerPoint > bankerPoint) {
                        result = "Thắng";
                    }
                    if (playerPoint < bankerPoint) {
                        result = "Thua";
                    }
                    if (playerPoint == bankerPoint) {
                        result = "Hòa";
                    }
                }
            }
            player.total = playerPoint;
            player.result = result;
            if (result == "Hòa") {
                player.recieve = 0;
                player.trueRecieve = 0;
            }
        }
        const totalWinBanker = game.players.reduce((prev, player) => {
            if (player.result == "Thua") {
                return prev + Number(player.totalBet);
            }
            return prev
        }, 0);
        console.log("totalWinBanker", totalWinBanker)
        let moneyBanker = Number(banker.money);
        if (totalWinBanker > 0) {
            if (moneyBanker >= totalWinBanker) {
                moneyBanker += totalWinBanker;
                for (const player of game.players) {
                    if (player.result == "Thua") {
                        player.recieve = - Number(player.totalBet);
                        player.trueRecieve = player.recieve;
                    }
                }
            }
            else {
                for (const player of game.players) {
                    if (player.result == "Thua") {
                        player.recieve = -Math.floor((Number(player.totalBet) / totalWinBanker) * moneyBanker);
                        player.trueRecieve = player.recieve;
                    }
                }
                moneyBanker += moneyBanker;
            }
        }
        const totalLossBanker = game.players.reduce((prev, player) => {
            if (player.result == "Thắng") {
                return prev + Number(player.totalBet);
            }
            return prev
        }, 0);
        console.log("totalLossBanker", totalLossBanker)
        if (totalLossBanker > 0) {
            if (moneyBanker >= totalLossBanker) {
                moneyBanker -= totalLossBanker;
                for (const player of game.players) {
                    if (player.result == "Thắng") {
                        player.recieve = Number(player.totalBet);
                        // player.trueRecieve = Math.floor((1 - Number(game.pay)) * player.recieve);
                        player.trueRecieve = player.recieve - Math.floor(Number(game.pay) * player.recieve);
                    }
                }
            }
            else {
                for (const player of game.players) {
                    if (player.result == "Thắng") {
                        player.recieve = Math.floor((Number(player.totalBet) / totalLossBanker) * moneyBanker);
                        player.trueRecieve = player.recieve - Math.floor(Number(game.pay) * player.recieve);
                    }
                }
                moneyBanker -= moneyBanker;
            }
        }
        const bankerWin = moneyBanker - Number(banker.money);
        banker.recieve = bankerWin;
        banker.trueRecieve = bankerWin > 0 ? bankerWin - Math.floor(Number(game.pay) * bankerWin) : bankerWin;
        banker.lastMoney = Number(banker.money) + banker.trueRecieve;
        for (const player of game.players) {
            player.lastMoney = Number(player.money) + player.trueRecieve;
        }
        setData([...data]);
    }

    const save = () => {
        localStorage.setItem("history_", JSON.stringify(data));
    }

    const exportFile = () => {
        save();
        const fileData = JSON.stringify(data);
        const blob = new Blob([fileData], { type: "text/plain" });
        const url = URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.download = "data.txt";
        link.href = url;
        link.click();
    }

    const importFile = (e) => {
        e.preventDefault()
        const reader = new FileReader()
        reader.onload = async (e) => {
            const text = (e.target.result)
            const history = JSON.parse(text);
            setData([...data, ...history]);
        };
        reader.readAsText(e.target.files[0])
        e.target.files = null
    }

    return (
        <div className={cx("container")}>
            <div className={cx("content")}>
                {data.map((game, index) => (
                    <Game
                        onChangeBet={onChangeBet}
                        onChangePay={onChangePay}
                        onChangeDes={onChangeDes}
                        onChangeCard1Banker={onChangeCard1Banker}
                        onChangeCard2Banker={onChangeCard2Banker}
                        onChangeCard3Banker={onChangeCard3Banker}
                        onChangeMoneyBanker={onChangeMoneyBanker}
                        onChangeCard1Player={onChangeCard1Player}
                        onChangeCard2Player={onChangeCard2Player}
                        onChangeCard3Player={onChangeCard3Player}
                        onChangeMoneyPlayer={onChangeMoneyPlayer}
                        onChangeBetUser={onChangeBetUser}
                        calculate={calculate}
                        onDelete={onDelete}
                        onDeletePlayer={onDeletePlayer}
                        onAddPlayer={onAddPlayer}
                        onCopy={onCopy}
                        key={index}
                        game={game} />
                ))}
            </div>
            <div className={cx("footer")}>
                <div style={{
                    display: "flex",
                    flexDirection: "row"
                }}>
                    <div style={{ marginRight: 12 }}>Số người chơi</div>
                    <input
                        value={numberPlayer}
                        type={"number"}
                        onChange={e => setNumberPlayer(e.target.value)} />
                </div>
                <div
                    style={{
                        display: "flex",
                        flexDirection: "row",
                        marginBottom: 20
                    }}>
                    <button
                        onClick={addNewGame}
                        style={{ marginTop: 10 }}>Game mới</button>
                    <button
                        onClick={save}
                        style={{ marginTop: 10, marginLeft: 20 }}>Lưu</button>
                </div>
                <div
                    style={{
                        display: "flex",
                        flexDirection: "row",
                        marginBottom: 20
                    }}>
                    <input
                        onChange={importFile}
                        style={{ marginTop: 10 }}
                        type={"file"} />
                    <button
                        onClick={exportFile}
                        style={{ marginTop: 10, marginLeft: 20 }}>Xuất file</button>
                </div>
            </div>
        </div>
    );
}

export default Home