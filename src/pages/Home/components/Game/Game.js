import classNames from "classnames/bind";

import styles from "./Game.module.scss";

const cx = classNames.bind(styles);

function Game({
    game,
    onChangeBet,
    onChangePay,
    onChangeDes,
    onChangeCard1Banker,
    onChangeCard2Banker,
    onChangeCard3Banker,
    onChangeMoneyBanker,
    onChangeCard1Player,
    onChangeCard2Player,
    onChangeCard3Player,
    calculate,
    onChangeMoneyPlayer,
    onChangeBetUser,
    onDelete,
    onCopy,
    onDeletePlayer,
    onAddPlayer
}) {

    return (
        <div className={cx("wrapper")}>
            <div
                style={{
                    display: "flex",
                    flexDirection: "row",
                    marginTop: 4,
                    marginLeft: 4
                }}>
                <div>Nội dung:  </div>
                <textarea
                    className={cx("des")}
                    value={game.des}
                    onChange={e => onChangeDes(game, e.target.value)}>

                </textarea>
            </div>
            <div className={cx("action")}>
                <div className={cx("copy")}
                    onClick={() => onCopy(game)}>Copy</div>
                <div className={cx("delete")}
                    onClick={() => onDelete(game.id)}>Xóa</div>
            </div>
            <div className={cx("infor")}>
                <div className={cx("level")}>Mức phế</div>
                <input
                    style={{ width: 100, textAlign: "center" }}
                    value={game.pay}
                    type={"number"}
                    onChange={e => onChangePay(game, e.target.value)} />
                <div className={cx("level2")}>Mức cược</div>
                <input
                    style={{ width: 100, textAlign: "center" }}
                    value={game.bet}
                    type={"number"}
                    onChange={e => onChangeBet(game, e.target.value)} />
            </div>
            <div className={cx("top")}>
                <div className={cx("flex-2")}>
                    Name
                </div>
                <div className={cx("flex-1")}>
                    Lá 1
                </div>
                <div className={cx("flex-1")}>
                    Lá 2
                </div>
                <div className={cx("flex-1")}>
                    Lá 3
                </div>
                <div className={cx("flex-1")}>
                    Điểm
                </div>
                <div className={cx("flex-1")}>
                    Kết quả
                </div>
                <div className={cx("flex-2")}>
                    Tiền ban đầu
                </div>
                <div className={cx("flex-2")}>
                    Tổng cược
                </div>
                <div className={cx("flex-2")}>
                    Nhận
                </div>
                <div className={cx("flex-2")}>
                    Thực nhận
                </div>
                <div className={cx("flex-2")}>
                    Kết thúc
                </div>
            </div>
            <div className={cx("banker")}>
                <div className={cx("flex-2")}>
                    Nhà cái
                </div>
                <div className={cx("flex-1")}>
                    <input
                        type={"number"}
                        style={{
                            width: 50,
                            textAlign: "center"
                        }}
                        value={game.banker.card1}
                        onChange={e => onChangeCard1Banker(game, e.target.value)}
                    />
                </div>
                <div className={cx("flex-1")}>
                    <input
                        type={"number"}
                        style={{
                            width: 50,
                            textAlign: "center"
                        }}
                        value={game.banker.card2}
                        onChange={e => onChangeCard2Banker(game, e.target.value)}
                    />
                </div>
                <div className={cx("flex-1")}>
                    {((Number(game.banker.card1) + Number(game.banker.card2)) % 10 != 9) &&
                        <input
                            type={"number"}
                            style={{
                                width: 50,
                                textAlign: "center"
                            }}
                            value={game.banker.card3}
                            onChange={e => onChangeCard3Banker(game, e.target.value)}
                        />
                    }
                </div>
                <div className={cx("flex-1")}>
                    {game.banker.total}
                </div>
                <div className={cx("flex-1")}>
                    {game.banker.result}
                </div>
                <div className={cx("flex-2")}>
                    <input
                        type={"number"}
                        style={{
                            width: 100,
                            textAlign: "center"
                        }}
                        value={game.banker.money}
                        onChange={e => onChangeMoneyBanker(game, e.target.value)}
                    />
                </div>
                <div className={cx("flex-2")}>

                </div>
                <div className={cx("flex-2")}>
                    {game.banker.recieve}
                </div>
                <div className={cx("flex-2")}>
                    {game.banker.trueRecieve}
                </div>
                <div className={cx("flex-2")}>
                    {game.banker.lastMoney}
                </div>
            </div>
            {game.players?.map((player, index) => (
                <div
                    key={index}
                    className={cx("player")}>
                    <div className={cx("flex-2")}>
                        {`Người chơi ${index + 1}`}
                    </div>
                    <div className={cx("flex-1")}>
                        <input
                            type={"number"}
                            style={{
                                width: 50,
                                textAlign: "center"
                            }}
                            value={player.card1}
                            onChange={e => onChangeCard1Player(player, e.target.value)}
                        />
                    </div>
                    <div className={cx("flex-1")}>
                        <input
                            type={"number"}
                            style={{
                                width: 50,
                                textAlign: "center"
                            }}
                            value={player.card2}
                            onChange={e => onChangeCard2Player(player, e.target.value)}
                        />
                    </div>
                    <div className={cx("flex-1")}>
                        {((Number(player.card1) + Number(player.card2)) % 10 != 9) &&
                            <input
                                type={"number"}
                                style={{
                                    width: 50,
                                    textAlign: "center"
                                }}
                                value={player.card3}
                                onChange={e => onChangeCard3Player(player, e.target.value)}
                            />
                        }
                    </div>
                    <div className={cx("flex-1")}>
                        {player.total}
                    </div>
                    <div className={cx("flex-1")}>
                        {player.result}
                    </div>
                    <div className={cx("flex-2")}>
                        <input
                            type={"number"}
                            style={{
                                width: 100,
                                textAlign: "center"
                            }}
                            value={player.money}
                            onChange={e => onChangeMoneyPlayer(player, e.target.value)}
                        />
                    </div>
                    <div className={cx("flex-2")}>
                        <input
                            type={"number"}
                            style={{
                                width: 100,
                                textAlign: "center"
                            }}
                            value={player.totalBet}
                            onChange={e => onChangeBetUser(player, e.target.value)}
                        />
                    </div>
                    <div className={cx("flex-2")}>
                        {player.recieve}
                    </div>
                    <div className={cx("flex-2")}>
                        {player.trueRecieve}
                    </div>
                    <div className={cx("flex-2")}>
                        {player.lastMoney}
                    </div>
                    <div
                        style={{
                            display: game.players.length == 1 ? "none" : "block"
                        }}
                        className={cx("delete-player")}
                        onClick={() => onDeletePlayer(game, player.id)}>Xóa</div>
                </div>
            ))}
            {game.players.length < 5 &&
                <button
                    style={{
                        marginLeft: 30
                    }}
                    onClick={() => onAddPlayer(game)}>Thêm</button>
            }
            <div className={cx("bottom")}>
                <button onClick={() => calculate(game)}>
                    Tính
                </button>
            </div>
        </div>
    )
}

export default Game